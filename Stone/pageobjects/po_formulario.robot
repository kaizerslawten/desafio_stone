*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  BuiltIn
Library  Collections
Library  String

Library   ../keywords/keywords.py
Resource   ../keywords/keyword.robot

Variables  ../masstests/ct001_stone.py
Variables  ../dictionary/dictionary.py


*** Keywords ***

Setup
    Maximize Browser Window

Navigate to form
    Open Browser    ${url1}  ${Browser}
    Setup

Fill form fields
    Click Element Dictionary  ${form_btn_start}

    Input Element Dictionary  ${form_edt_first_name}  ${first_name}
    PressKeys Dictionary  ${form_edt_first_name}  RETURN

    Input Element Dictionary  ${form_edt_last_name}  ${last_name}
    PressKeys Dictionary  ${form_edt_last_name}  RETURN

    Input Element Dictionary  ${form_edt_email}  ${email}
    PressKeys Dictionary  ${form_edt_email}  RETURN

    Input Element Dictionary  ${form_edt_born_country}  ${born}
    PressKeys Dictionary  ${form_edt_born_country}  RETURN

Fill invalid form fields
    Click Element Dictionary  ${form_btn_start}

    Input Element Dictionary  ${form_edt_first_name}  ${first_name}
    PressKeys Dictionary  ${form_edt_first_name}  RETURN

    Input Element Dictionary  ${form_edt_last_name}  ${last_name}
    PressKeys Dictionary  ${form_edt_last_name}  RETURN

    Input Element Dictionary  ${form_edt_email}  ${email2}

Click Enter Email
    PressKeys Dictionary  ${form_edt_email}  RETURN

Click Register User
    Click Element Dictionary  ${form_btn_register}

Click Next Button
    Click Element Dictionary  ${form_btn_next}

Verify If User was Registered
    Verify text in element  ${form_sucess}  ${value_sucess}

Verify Invalid Fields
    Verify text in element  ${form_failed}  ${value_fail}

Verify Invalid Register
    Verify text in element  ${form_invalid_data}  ${value_invalid_register}

#Métodos usados pelo teste em BDD
que esteja pagina de cadastro
    Navigate to form

preencher as informacoes do usuario
    fill form fields

submeter o formulario
    Click Register User

a mensagem de confirmacao "${MENSAGEM}" sera exibida
    Verify text in element  ${form_sucess}  ${MENSAGEM}