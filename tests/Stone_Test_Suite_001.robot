*** Settings ***
Library             SeleniumLibrary
Resource            ../Stone/pageobjects/po_formulario.robot
Resource            ../bdd/BDD_pt_br.robot
Test Teardown       Close Browser

*** Variables ***

*** Test Cases ***

CT 01 : Preencher Formulario e Registrar usuario

    Navigate to form

    Fill form fields

    Click Register User

    Verify If User was Registered


CT 02 : Preencher Formulario com e-mail invalido

    Navigate to form

    Fill invalid form fields

    Click Enter Email

    Verify Invalid Fields


CT 03 : Tentar regitrar usuario com dados invalidos

    Navigate to form

    Fill invalid form fields

    Click Next Button

    Click Next Button

    Click Register User

    Verify Invalid Register


#Implementação BDD em PT-Br
Cenario 04: Cadastrar novo usuario
    Dado  que esteja pagina de cadastro
    E  preencher as informacoes do usuario
    Quando  submeter o formulario
    Entao  a mensagem de confirmacao "Thanks for completing this typeform" sera exibida
