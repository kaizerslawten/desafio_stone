
*** Settings ***
Library  SeleniumLibrary

*** Keywords ***
Click Element Dictionary
    [Documentation]  Function is responsible for click in element given dictionary key
    [Arguments]  ${dictionary}
    Set Selenium Speed  0.1
    wait until element is visible  ${dictionary['locator']}:${dictionary['value']}  40
    Click Element  ${dictionary['locator']}:${dictionary['value']}

Input Element Dictionary
    [Documentation]  Function input data in element given dictionary key and text
    [Arguments]  ${dictionary}  ${data}
    wait until element is visible  ${dictionary['locator']}:${dictionary['value']}  40
    Input Text  ${dictionary['locator']}:${dictionary['value']}  ${data}

PressKeys Dictionary
    [Documentation]  Function is responsible for press key
    [Arguments]  ${dictionary}  ${key}
    wait until element is visible  ${dictionary['locator']}:${dictionary['value']}  40
    Press Keys  ${dictionary['locator']}:${dictionary['value']}  ${key}

Verify text in element
    [Documentation]  Function is responsible for check if value exists in element
    [Arguments]  ${dictionary}  ${value}
    wait until element is visible  ${dictionary['locator']}:${dictionary['value']}  40
    Element Should Contain  ${dictionary['locator']}:${dictionary['value']}  ${value}



