from robot.api.deco import keyword
from robot.libraries.BuiltIn import BuiltIn

seleniumlib = BuiltIn().get_library_instance('SeleniumLibrary')

@keyword('Element')
def login(locator):
    return locator['locator']+":"+locator['value']

@keyword('Clicar Elemento')
def clicarElement(locator):
    seleniumlib.click_element(locator['locator']+":"+locator['value'])

