#Alterar entre navegadores
Browser = "chrome"
url1 = "https://stonepayments.typeform.com/to/yl5PKW"
form_btn_start = {"locator": "xpath", "value": "//*/div[@class='_Text-sc-1t2ribu-0-div ctXGkP' and text()='start']"}
form_edt_first_name = {"locator": "xpath", "value": "//*/div[@class='sc-kAzzGY gtjlFr']//strong[text()=' first name']//following::input[@placeholder='Type your answer here...'][1]"}
form_edt_last_name = {"locator": "xpath", "value": "//*/div[@class='sc-kAzzGY gtjlFr']//strong[text()=' first name']//following::input[@placeholder='Type your answer here...'][2]"}
form_edt_email = {"locator": "xpath", "value": "//*/input[@placeholder='Type your email here...']"}
form_edt_born_country = {"locator": "xpath", "value": "//*/input[@placeholder='Type or select an option']"}
form_btn_register = {"locator": "xpath", "value": "//*/div[text()='Register']"}
form_btn_review = {"locator": "xpath", "value": "//*/div[text()='Review']"}
form_btn_ok = {"locator": "xpath", "value": "//div[text()='OK']"}
form_sucess = {"locator": "xpath", "value": "//h1"}
form_failed = {"locator": "xpath", "value": "//div[@class='_Text-sc-1t2ribu-0-div eZDCIe' and contains(text(),'Hmm')]"}
form_btn_next = {"locator": "xpath", "value": "//button[@data-qa='fixed-footer-navigation-next']"}
form_invalid_data = {"locator": "xpath", "value": "//div[text()='1 answer needs completing']"}

def locator(element):
    return element['locator']+':'+element['value']
