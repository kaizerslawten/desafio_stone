*** Keywords ***

Dado
    [Arguments]  ${keyword}
    run keyword  ${keyword}

Quando
    [Arguments]  ${keyword}
    run keyword  ${keyword}

Entao
    [Arguments]  ${keyword}
    run keyword  ${keyword}

E
    [Arguments]  ${keyword}
    run keyword  ${keyword}

