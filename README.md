## Antes de começar, você precisará configurar o ambiente virtual

1. Clonar este repositório
2. **Abrir um terminal em modo administrador**
3. **Caso não tenha instalado**, instalar o VirtualEnv via **"pip install virtualenv"**
4. Na raiz do projeto crie um ambiente virtual, execute o comando **"virtualenv venv"**
5. Ativar o ambiente virtual, execute o comando **"venv\Scripts\activate"**

---

## Dependências do projeto

- [robotframework](https://robotframework.org/) (Framework de Automação)
- [robotframework-metrics](https://github.com/adiralashiva8/robotframework-metrics) (Plugin para geração de relatórios)
- [robotframework-seleniumlibrary](http://robotframework.org/SeleniumLibrary/SeleniumLibrary.html) (Biblioteca Robot Selenium para testes Web)

O gerenciamento de dependências é feito pelo programa **pip** no arquivo **requirements.txt**.

Na raíz do projeto executar o comando "**pip install -r requirements.txt**" para satisfazer as dependências do projeto.

---

##  Capacidades do projeto

- Desenvolvimento otimizado de testes automatizados, visto que todo o trabalho pesado de comandos selenium são feitos pela Biblioteca **"SeleniumLibrary"** do [robotframework](https://robotframework.org/)
- Permite suporte a diversos WebDrivers de navegadores para execução do testes
- Permite escrita de testes BDD PT-BR e Inglês
- Provê relatório automatizado após as execuções das suites de testes

##  Iniciar Testes

Para iniciar a execução das suite de testes, na raiz do projeto deve ser executado o comando "**RunTests.bat tests\Stone_Test_Suite_001.robot**"

##  Relatórios e métricas de execução
Após toda execução de suite de teste, são gerados relatórios das execuções, com o auxilio do plugin "[robotframework-metrics](https://github.com/adiralashiva8/robotframework-metrics)",
é gerado um excelente relatório com gráficos, métricas de performance e acuracidade dos testes. 

Após a execução na raíz do projeto, o arquivo de relatório gerado  possui o nome seguindo o exemplo abaixo:

**"metrics-20190616-002815.html"**